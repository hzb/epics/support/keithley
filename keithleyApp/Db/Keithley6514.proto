##########################################################################
# Copyright (c) 2011 Lawrence Berkeley National Laboratory, Accelerator
#                    Technology Group, Engineering Division
# Copyright (c) 2018 Helmholtz-Zentrum Berlin für Materialien           
#                    und Energie GmbH, Germany (HZB)
# This code is distributed subject to a Software License Agreement found
# in file LICENSE that is included with this distribution.
# NOTE: This is a strongly modified HZB version of the original driver.
##########################################################################

LockTimeout = 7500;
ReadTimeout = 2000; 
InTerminator = "\r";
OutTerminator = "\r";



WaitDelay    = 40;
WriteTimeout = 500;
ReplyTimeout = 2000;
#ReadTimeout = 500;

####
## GENERAL
####
init {
    out "FORM:DATA ASC"; out "FORM:SREG ASC"; out "FORM:SOUR2 ASC";
    out "FORM:ELEM READ"; out "FORM:BORD NORMAL"; 
    out "CONF:CURR:DC"; out "TRIG:COUN 1";
}

cmd { out "\$1";}
setDec { out "\$1 %d"; }
getDec { out "\$1?"; in "%d"; ExtraInput = Ignore; }
setFlt { out "\$1 %f"; }
getFlt { out "\$1?"; in "%f"; ExtraInput = Ignore; }
setStr { out "\$1 %s"; }
getStr { out "\$1?"; in "%s"; ExtraInput = Ignore; }

####
## COMMON
####
getIDN {out "*IDN?"; in "%\$1[^\r\n]"; ExtraInput = Ignore;}
getIDNString {out "*idn?";in "KEITHLEY INSTRUMENTS INC.,MODEL %d%*s";
}

####
## CALCulate2 (Limit Tests configuration)
####
setLimitTestInp {out "CALC2:FEED: %{SENS|CALC}"; }
getLimitTestInp {out "CALC2:FEED?"; in "%{SENS|CALC}"; ExtraInput = Ignore;}

####
## SENSe
####
setRangeCur { out "\$1 2.1E-%d"; }
getRangeCur { out "\$1?"; in "%*[^E]E-%d"; ExtraInput = Ignore; }
setRangeVolt { out "\$1 %d"; }
getRangeVolt { out "\$1?"; in "%d.%*s"; }
setRangeRes { out "\$1 2.1E+%d"; }
getRangeRes { out "\$1?"; in "%*[^E]E+%d"; ExtraInput = Ignore; }
setRangeChar { out "\$1 2.1E-%d"; }
getRangeChar { out "\$1?"; in "%*[^E]E-%d"; ExtraInput = Ignore; }

####
## TRIGger
####
getTrigSour { out "TRIG:SOUR?"; in "%{IMM|TLIN}"; ExtraInput = Ignore;}
setTrigSour { out "TRIG:SOUR %{IMM|TLIN}"; }

getTrigOut { out "TRIG:OUTP?"; in "%{NONE|SENS}"; ExtraInput = Ignore;}
setTrigOut { out "TRIG:OUTP %{NONE|SENS}"; }

####
## ARM
####
setArmSour {out "ARM:SOUR %{IMM|BUS|TIM|MAN|TLIN|NST|PST|BST}"; }
getArmSour {out "ARM:SOUR?"; in "%{IMM|BUS|TIM|MAN|TLIN|NST|PST|BST}"; ExtraInput = Ignore;}

####
## MEASUREMENT
####
setFunc {out "FUNC %{\"CURR:DC\"|\"VOLT:DC\"|\"RES\"|\"CHAR\"}"; }
getFunc {out "FUNC?"; in "%{\"CURR:DC\"|\"VOLT:DC\"|\"RES\"|\"CHAR\"}"; ExtraInput = Ignore;}

#trigger and fetch fresh reading
read {
    out "read?";
    in "%f";

    # Repeat the reading if it failed
    @readtimeout{
        out "READ?";
        in "%f";}
}

#use this if TRIG:COUN > 1
#read2 {
#    ReplyTimeout = 10000;
#    Separator = ",";
#    out "READ?";
#    in "%f";
#    ExtraInput = Ignore;
#}

## Integration time in PowerLineCycles
setIntegrTime {
    out "CURR:NPLC %f";
    out "VOLT:NPLC %f";
    out "RES:NPLC %f";
    out "CHAR:NPLC %f";
}

setAutoRange {
    out "CURR:RANG:AUTO %d";
    out "VOLT:RANG:AUTO %d";
    out "RES:RANG:AUTO %d";
    out "CHAR:RANG:AUTO %d";
}

####
## SYSTem
####
getSystVers {out "SYST:VERS?"; in "%\$1[^\r\n]"; ExtraInput = Ignore;}
getSystErrAll {out "SYST:ERR:ALL?"; in "%\$1[^\r\n]";}

getSystErrNxt {out "SYST:ERR:NEXT?";in '%(\$1:systErrNum.VAL)d,"%255[^"]"';}
getSystErrCnt {out "SYST:ERR:COUN?";in "%d";}

####
## DEBUGING
####
# Connect a stringout record to this to get a generic command interface.
# After processing finishes, the record contains the reply.
debugCmd {
    out "%#s";
}

debugRbk {
    in "%#s";
}

