Forked from http://repo.acc.bessy.de/darcs/epics/support/keithley/

Additional Features:

- A single PV for setting the range with options for auto or fixed ranges
- Continuous updating of the range readback to monitor for range changes in auto range mode
- Support for 6517B Model with voltage output and slightly different range settings
- Focus on current measurement rather than a general multimeter
